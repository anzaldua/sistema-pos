<?php

	require_once "conexion.php";

	class ModeloCategoria{

		/*=============================================
		=            CREAR CATEGORIA            =
		=============================================*/
		
		static public function mdlCrearCategoria($tabla, $datos){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(categoria) values (:categoria)");

			$stmt -> bindParam(":categoria",$datos, PDO::PARAM_STR);

			if($stmt->execute()){

			return "ok";	

			}else{

				return "error";
			
			}

			$stmt->close();
			
			$stmt = null;
		}

	/*=============================================
	MOSTRAR USUARIOS
	=============================================*/

	static public function mdlMostrarCategoria($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		

		$stmt -> close();

		$stmt = null;

		}

		/*=============================================
		=            CREAR CATEGORIA            =
		=============================================*/
		
		static public function mdlEditarCategoria($tabla, $datos){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET categoria = :categoria WHERE id = :id");

			$stmt -> bindParam(":categoria",$datos["categoria"], PDO::PARAM_STR);
			$stmt -> bindParam(":id",$datos["id"], PDO::PARAM_STR);

			if($stmt->execute()){

			return "ok";	

			}else{

				return "error";
			
			}

			$stmt->close();
			
			$stmt = null;
		}

/*=============================================
=            ELIMINAR CATEGORIA            =
=============================================*/

		static public function mdlEliminarCategoria($tabla, $datos){

			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

			$stmt -> bindParam(":id",$datos, PDO::PARAM_STR);

			if($stmt->execute()){

			return "ok";	

			}else{

				return "error";
			
			}

			$stmt->close();
			
			$stmt = null;
		}
	}