<?php


	class ControladorCategoria{

		/*=============================================
		=            CREAR CATEGORIA            =
		=============================================*/
		
		static public function ctrCrearCategoria(){

			if(isset($_POST["nuevaCategoria"])){

				if(preg_match('/^[a-zA-Z0-9-ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaCategoria"])){

					$tabla = "categorias";

					$datos = $_POST["nuevaCategoria"];

					$respuesta = ModeloCategoria::mdlCrearCategoria($tabla, $datos);

					if($respuesta == "ok"){

					echo '<script>

					swal({

						type: "success",
						title: "¡La categoria ha sido guardado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "categorias";

						}

					});
				

					</script>';


				}else{

					echo '<script>

					swal({

						type: "error",
						title: "¡La categoria no puede ir vacío o llevar caracteres especiales!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "categorias";

						}

					});
				

				</script>';	


					}

				}

			}

		}

	/*=============================================
	MOSTRAR CATEGORIA
	=============================================*/

	static public function ctrMostrarCategoria($item, $valor){

		$tabla = "categorias";

		$respuesta = ModeloCategoria::mdlMostrarCategoria($tabla, $item, $valor);

		return $respuesta;
	}



/*=============================================
=            EDITAR CATEGORIA            =
=============================================*/

	static public function ctrEditarCategoria(){

			if(isset($_POST["editarCategoria"])){

				if(preg_match('/^[a-zA-Z0-9-ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarCategoria"])){

					$tabla = "categorias";

					$datos = array("categoria"=>$_POST["editarCategoria"], "id"=>$_POST["idCategoria"]);

					$respuesta = ModeloCategoria::mdlEditarCategoria($tabla, $datos);

					if($respuesta == "ok"){

					echo '<script>

					swal({

						type: "success",
						title: "¡La categoria ha sido modificada correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "categorias";

						}

					});
				

					</script>';


				}else{

					echo '<script>

					swal({

						type: "error",
						title: "¡La categoria no puede ir vacío o llevar caracteres especiales!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "categorias";

						}

					});
				

				</script>';	


					}

				}

			}

		}

/*=============================================
=            ELIMINAR CATEGORIA            =
=============================================*/

		static public function ctrEliminarCategoria(){

			if(isset($_GET["idCategoria"])){

				$tabla = "categorias";

				$datos = $_GET["idCategoria"];

				$respuesta = ModeloCategoria::mdlEliminarCategoria($tabla, $datos);

				if($respuesta == "ok"){

					echo '<script>

					swal({

						type: "success",
						title: "¡La categoria ha sido eliminada correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "categorias";

						}

					});
				

					</script>';


				}
			}

		}

	}