/*=============================================
=          REVISAR CATEGORIA REPETIDA          =
=============================================*/

$(document).on("change", "#nuevaCategoria", function(){

	$(".alert").remove();

	var categoria = $(this).val();

	var datos = new FormData();
	datos.append("validarCategoria", categoria);

	$.ajax({

	 	url:"ajax/categorias.ajax.php",
	 	method: "POST",
	 	data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			if(respuesta){

				$("#nuevaCategoria").parent().after('<div class="alert alert-warning">Esta categoria ya existe</div>');

				$("#nuevaCategoria").val("");
			}
		}
	 })
})

/*=============================================
=          MODIFICAR CATEGORIA          =
=============================================*/

$(document).on("click", ".btnEditarCategoria", function(){

		var idCategoria = $(this).attr("idCategoria");
		
		var datos = new FormData();
		datos.append("idCategoria", idCategoria);

		$.ajax({

			url:"ajax/categorias.ajax.php",
			method: "POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "json",
			success: function(respuesta){

				$("#editarCategoria").val(respuesta["categoria"]);
				$("#idCategoria").val(respuesta["id"]);

			}

		});

	})

/*=============================================
=          ELIMINAR CATEGORIA          =
=============================================*/

$(document).on("click", ".btnEliminarCategoria", function(){

		var idCategoria = $(this).attr("idCategoria");
		
		swal({
		title: '¿Estas seguro que deseas eliminar la categoria?',
		text: "¡Si no lo está, cancele la accion!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtontext: 'Cancelar',
		confirmButtonText: 'Si, borrar categoria'
	}).then(function(result){

		if(result.value){

			window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;

		}

	})

})