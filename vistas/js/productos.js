
/*=============================================
= CAPTURANDO LA CATEGORIA PARA ASIGNAR CODIGO =
=============================================*/

$("#nuevaCategoria").change(function(){

	var idCategoria = $(this).val();

	var datos = new FormData();
	datos.append("idCategoria", idCategoria);

	$.ajax({

		url:"ajax/productos.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success:function(respuesta){
			
			if(!respuesta){

				var nuevoCodigo = idCategoria+"01";
				$("#nuevoCodigo").val(nuevoCodigo);

			}else{
			var nuevoCodigo = Number(respuesta["codigo"])+1;
			$("#nuevoCodigo").val(nuevoCodigo);
			}
		}
	})

})

/*=============================================
=            AGREGANDO PRECIO VENTA            =
=============================================*/

$("#nuevoPrecioCompra").change(function(){

	if($(".porcentaje").prop("checked")){

	var valorPorcentaje = $(".nuevoPorcentaje").val();
	var porcentaje = Number(($("#nuevoPrecioCompra").val()*valorPorcentaje/100))+Number($("#nuevoPrecioCompra").val());

	$("#nuevoPrecioVenta").val(porcentaje);
	$("#nuevoPrecioVenta").prop("readonly",true);

	}


})

/*=============================================
=            AGREGANDO PRECIO VENTA            =
=============================================*/

$(".nuevoPorcentaje").change(function(){

	if($(".porcentaje").prop("checked")){

	var valorPorcentaje = $(".nuevoPorcentaje").val();
	var porcentaje = Number(($("#nuevoPrecioCompra").val()*valorPorcentaje/100))+Number($("#nuevoPrecioCompra").val());

	$("#nuevoPrecioVenta").val(porcentaje);
	$("#nuevoPrecioVenta").prop("readonly",true);

	}


})


$(".porcentaje").on("ifUnchecked", function(){

	$("#nuevoPrecioVenta").prop("readonly",false);

})

$(".porcentaje").on("ifChecked", function(){

	$("#nuevoPrecioVenta").prop("readonly",true);

})

/*=============================================
=    SUBIENDO LA FOTO DEL USUARIOS            =
=============================================*/
		$(".nuevaImagen").change(function(){


			var imagen = this.files[0];

/*=============================================
=    VALIDANDO FORMATO DE LA IMAGEN            =
=============================================*/

		if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

			$(".nuevaImagen").val("");

				swal({

					title: "Error al subir la imagen",
					text: "¡La imagen debe estar en formato JPG o PNG!",
					type: "error",
					confirmButtonText: "¡Cerrar!"
				})

			}else if(imagen["size"] > 2000000){
				swal({

					title: "Error al subir la imagen",
					text: "¡La imagen no debe superar los 2 megabytes!",
					type: "error",
					confirmButtonText: "¡Cerrar!"
				})
			}else{
				var datosImagen = new FileReader;
				datosImagen.readAsDataURL(imagen);

				$(datosImagen).on("load", function(event){

					var rutaImagen = event.target.result;

					$(".previsualizarImagenDefault").attr("src", rutaImagen);
				})
			}
	})

/*=============================================
EDITAR PRODUCTO
=============================================*/

$(".tablaProductos").on("click", "button.btnEditarProducto", function(){

	var idProducto = $(this).attr("idProducto");
	
	var datos = new FormData();
    datos.append("idProducto", idProducto);

     $.ajax({

      url:"ajax/productos.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(respuesta){
          
          var datosCategoria = new FormData();
          datosCategoria.append("idCategoria",respuesta["id_categoria"]);

           $.ajax({

              url:"ajax/categorias.ajax.php",
              method: "POST",
              data: datosCategoria,
              cache: false,
              contentType: false,
              processData: false,
              dataType:"json",
              success:function(respuesta){
                  
                  $("#editarCategoria").val(respuesta["id"]);
                  $("#editarCategoria").html(respuesta["categoria"]);

              }

          })

           $("#editarCodigo").val(respuesta["codigo"]);

           $("#editarDescripcion").val(respuesta["descripcion"]);

           $("#editarStock").val(respuesta["stock"]);

           $("#editarPrecioCompra").val(respuesta["precio_compra"]);

           $("#editarPrecioVenta").val(respuesta["precio_venta"]);

           if(respuesta["imagen"] != ""){

           	$("#imagenActual").val(respuesta["imagen"]);

           	$(".previsualizar").attr("src",  respuesta["imagen"]);

           }

      }

  })

})

/*=============================================
=            EDITANDO PRECIO VENTA            =
=============================================*/

$("#editarPrecioCompra").change(function(){

	if($(".porcentaje").prop("checked")){

	var valorPorcentaje = $(".EditarPorcentaje").val();
	var porcentaje = Number(($("#editarPrecioCompra").val()*valorPorcentaje/100))+Number($("#editarPrecioCompra").val());

	$("#editarPrecioVenta").val(porcentaje);
	$("#editarPrecioVenta").prop("readonly",true);

	}
})

$(".EditarPorcentaje").change(function(){

	if($(".porcentaje").prop("checked")){

	var valorPorcentaje = $(".EditarPorcentaje").val();
	var porcentaje = Number(($("#editarPrecioCompra").val()*valorPorcentaje/100))+Number($("#editarPrecioCompra").val());

	$("#editarPrecioVenta").val(porcentaje);
	$("#editarPrecioVenta").prop("readonly",true);

	}


})

$(".porcentaje").on("ifUnchecked", function(){

	$("#editarPrecioVenta").prop("readonly",false);

})

$(".porcentaje").on("ifChecked", function(){

	$("#editarPrecioVenta").prop("readonly",true);

})

/*=============================================
ELIMINAR PRODUCTO
=============================================*/

$(".tablaProductos").on("click", "button.btnEliminarProducto", function(){

	var idProducto = $(this).attr("idProducto");
	var codigo = $(this).attr("codigo");
	var imagen = $(this).attr("imagen");
	
	swal({

		title: '¿Está seguro de borrar el producto?',
		text: "¡Si no lo está puede cancelar la accíón!",
		type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar producto!'
        }).then(function(result){
        if (result.value) {

        	window.location = "index.php?ruta=productos&idProducto="+idProducto+"&imagen="+imagen+"&codigo="+codigo;

        }


	})

})