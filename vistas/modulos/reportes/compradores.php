<?php

    $item = null;
    $valor = null;
    $ventas = ControladorVentas::ctrMostrarVentas($item, $valor);

    $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);

    $arrayClientes = array();
    $arrayListaClientes = array();

    foreach ($ventas as $key => $valueVentas) {

      foreach ($clientes as $key => $valueClientes) {

        if($valueClientes["id"] == $valueVentas["id_cliente"]){

          //CAPTURAMOS LOS CLIENTES EN UN ARRAY
          array_push($arrayClientes, $valueClientes["nombre"]);

          //CAPUTRAMOS LOS NOMBRES Y LOS VALORES NETOS EN UN MISMO ARRAY
          $arrayListaClientes = array($valueClientes["nombre"] => $valueVentas["neto"]);


          //SUMAMOS LOS NETOS DE CADA CLIENTES PARA QUE NO SE REPITA

          foreach ($arrayListaClientes as $key => $value) {
            
            $sumaTotalClientes[$key] += $value;
          }
        }
      }
    }

  $noRepetirNombres = array_unique($arrayClientes);

?>

<!-- COMRPADORES -->

<div class="box box-primary">
	
	<div class="box-header with-border">
		
		<h3 class="box-title">Compradores</h3>
	
	</div>
	
	<div class="box-body">
		
		<div class="chart-responsive">
			
			<div class="chart" id="bar-chart" style="height:300px;"></div>

		</div>

	</div>

</div>

<script>
	
	//BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [
          <?php

          foreach ($noRepetirNombres as $value) {
            
            echo "{y: '".$value."', a: '".$sumaTotalClientes[$value]."'},";

          }

        ?>
      ],
      barColors: ['#0af'],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Compras'],
      preUnits: '$',
      hideHover: 'auto'
    });

</script>