<?php

	$item = null;
	$valor = null;
	$ventas = ControladorVentas::ctrMostrarVentas($item, $valor);
	$usuarios = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

	$arrayVendedores = array();
	$arrayListaVendedores = array();

	foreach ($ventas as $key => $valueVentas) {

		foreach ($usuarios as $key => $valueUsuarios) {

			if($valueUsuarios["id"] == $valueVentas["id_vendedor"]){

				//CAPTURAMOS LOS VENDEDORES EN UN ARRAY
				array_push($arrayVendedores, $valueUsuarios["nombre"]);

				//CAPUTRAMOS LOS NOMBRES Y LOS VALORES NETOS EN UN MISMO ARRAY
				$arrayListaVendedores = array($valueUsuarios["nombre"] => $valueVentas["neto"]);

				//SUMAMOS LOS NETOS DE CADA VENDEDOR PARA QUE NO SE REPITA

				foreach ($arrayListaVendedores as $key => $value) {
				
				$sumaTotalVendedores[$key] += $value;

				}
			}
		}
	}

	$noRepetirNombres = array_unique($arrayVendedores);

?>

<!-- VENDEDORES -->

<div class="box box-success">
	
	<div class="box-header with-border">
		
		<h3 class="box-title">Vendedores</h3>
	
	</div>
	
	<div class="box-body">
		
		<div class="chart-responsive">
			
			<div class="chart" id="bar-chart1" style="height:300px;"></div>

		</div>

	</div>

</div>

<script>
	
	//BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart1',
      resize: true,
      data: [

      <?php

      	foreach ($noRepetirNombres as $value) {
      		
      		echo "{y: '".$value."', a: '".$sumaTotalVendedores[$value]."'},";

      	}

      ?>
        
      ],
      barColors: ['#f6a'],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Compras'],
      preUnits: '$',
      hideHover: 'auto'
    });

</script>