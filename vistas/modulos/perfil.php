    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

  <!-- Main content -->

    <section class="content">
 
      <div class="row">

       <h1 style="float:left; padding-left: 10px; margin-top: -15px;">
        Perfil de Usuario
       </h1>

        <div class="col-md-4">

          <!-- Profile Image -->
          <div class="box box-primary" style="margin-left: 100px">
            <div class="box-body box-profile">
              <div class="user-panel">
                <div class="text-center rounded-circle image">
                  <?php
                    if($_SESSION["foto"] != ""){
                      echo '<img src="'.$_SESSION["foto"].'" class="user-image">';
                    }else{
                      echo '<img src="vistas/img/usuarios/default/anonimo.png" class="user-image">';
                    }
                  ?>  
                </div>
                
              </div>  

                <div class="text-center info">
                    <strong><span"><?php echo $_SESSION["nombre"]?></span></strong>
                </div>

                <div class="text-center info">
                    <strong><span"><?php echo $_SESSION["perfil"]?></span></strong>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary" style="margin-left: 100px">
            <div class="box-header with-border">
              <h3 class="box-title">Informacion</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-key margin-r-5"></i> Cambio de Contraseña</strong>

              <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Malibu, California</p>
  
              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
     </section>
    <!-- /.content -->
   </div>
  <!-- /.content-wrapper -->
  </div>
  
  