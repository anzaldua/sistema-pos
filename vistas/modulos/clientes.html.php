  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">

      <h1>

        Administracion de Clientes

      </h1>

      <ol class="breadcrumb">

        <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

        <li class="active">Administrar Cliente</li>

      </ol>

    </section>

    <section class="content">

      <!-- Default box -->
      <div class="box">

        <div class="box-header with-border">

            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCliente">
              Agregar Cliente
            </button>

        </div>

        <div class="box-body">

            <table class="table table-bordered table-striped tablas dt-responsive" width="100%">
              
                <thead>
                  
                    <tr>
                      <th style="width:10px">#</th>
                      <th>Nombre</th>
                      <th>Documento ID</th>
                      <th>Email</th>
                      <th>Telefono</th>
                      <th>Direccion</th>
                      <th>Fecha de Nacimiento</th>
                      <th>Total Compras</th>
                      <th>Ultima Compra</th>
                      <th>Ingreso al sistema</th>
                      <th>Acciones</th>
                    </tr>

                </thead>
                <tbody>
                  
                  <tr>
                    <td>1</td>
                    <td>Alex Cantu</td>
                    <td>8161123</td>
                    <td>alex_tigre_4@hotmail.com</td>
                    <td>555 57 67</td>
                    <td>San Patricio 1827</td>
                    <td>1982-15-11</td>
                    <td>35</td>
                    <td>2017-12-11 12:05:32</td>
                    <td>2017-12-11 12:05:32</td>
                    <td>
                      
                      <div class="btn-group">
                        
                          <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                          <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                      
                      </div>

                    </td>

                  </tr>

                </tbody>

            </table>

        </div>

      </div>

    </section>

  </div>

  <!--=====================================
  =       MODAL AGREGAR CLIENTES            =
  ======================================-->
  
<div id="modalAgregarCliente" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

          <!--=====================================
          =          CABEZA DEL MODAL            =
          ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar Cliente</h4>

        </div>

         <!--=====================================
          =         CUERPO DEL MODAL            =
          ======================================-->

        <div class="modal-body">

         <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->
           
            <div class="form-group">
              
              <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>

                  <input type="text" class="form-control input-lg" name="nuevoCliente" placeholder="Ingresar nombre" required>

              </div>

              <!-- ENTRADA PARA EL DOCUMENTO ID -->
           
            <div class="form-group">
              
              <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-key"></i></span>

                  <input type="number" min="0" class="form-control input-lg" name="nuevoDocumentoID" placeholder="Ingresar Documento" required>

              </div>

              <!-- ENTRADA PARA EL EMAIL -->
           
            <div class="form-group">
              
              <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

                  <input type="email" class="form-control input-lg" name="nuevoEmail" placeholder="Ingresar email" required>

            </div>

              <!-- ENTRADA PARA EL TELEFONO -->
           
            <div class="form-group">
              
              <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                  <input type="text" class="form-control input-lg" name="nuevoTelefono" placeholder="Ingresar telefono" data-inputmask="'mask':'(999) 999-9999'" data-mask required>

              </div>

              <!-- ENTRADA PARA LA DIRECCION -->
           
            <div class="form-group">
              
              <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>

                  <input type="email" class="form-control input-lg" name="nuevaDireccion" placeholder="Ingresar direccion" required>

            </div>

              <!-- ENTRADA PARA LA FECHA DE NACIMIENTO -->
           
            <div class="form-group">
              
              <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                  <input type="text" class="form-control input-lg" name="nuevaFechaNacimiento" placeholder="Ingresar fecha nacimiento" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask required>

              </div>

        <!-- PIE DE LAS ENTRADAS -->

        <div class="modal-footer">

          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar Cambios</button>

        </div>

      </form>

    </div>

  </div>

</div>
  
  