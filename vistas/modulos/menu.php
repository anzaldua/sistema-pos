<aside class="main-sidebar">
	<section class="sidebar">
		
		<ul class="sidebar-menu" data-widget="tree">
			<?php

		if($_SESSION["perfil"] == "Administrador"){

			if($_GET["ruta"] == "inicio"){
				echo '<li class="active">
						<a href="inicio">
							<i class="fa fa-home"></i>
							<span>Inicio</span>
						</a>
					  </li>';

			}else{

				echo '<li><a href="inicio">
						<i class="fa fa-home"></i>
						<span>Inicio</span>
					  </li></a>';
			}

			if($_GET["ruta"] == "usuarios"){
				echo '<li class="active">
						<a href="usuarios">
							<i class="fa fa-user"></i>
							<span>Usuarios</span>
						</a>
					  </li>';

			}else{

				echo '<li><a href="usuarios">
						<i class="fa fa-user"></i>
						<span>Usuarios</span>
					  </li></a>';
			}

		}

		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Especial"){

			if($_GET["ruta"] == "categorias"){
				echo '<li class="active">
						<a href="categorias">
							<i class="fa fa-th"></i>
							<span>Categorias</span>
						</a>
					  </li>';

			}else{

				echo '<li><a href="categorias">
						<i class="fa fa-th"></i>
						<span>Categorias</span>
					  </li></a>';
			}

			if($_GET["ruta"] == "productos"){
				echo '<li class="active">
						<a href="productos">
							<i class="fa fa-product-hunt"></i>
							<span>Productos</span>
						</a>
					  </li>';

			}else{

				echo '<li><a href="productos">
						<i class="fa fa-product-hunt"></i>
						<span>Productos</span>
					  </li></a>';
			}
		}

		if($_SESSION["perfil"] == "Administrador" || $_SESSION["perfil"] == "Vendedor"){

			if($_GET["ruta"] == "clientes"){
				echo '<li class="active">
						<a href="clientes">
							<i class="fa fa-users"></i>
							<span>Clientes</span>
						</a>
					  </li>';

			}else{

				echo '<li><a href="clientes">
						<i class="fa fa-users"></i>
						<span>Clientes</span>
					  </li></a>';
			}

			if($_GET["ruta"] == "ventas" || $_GET["ruta"] == "crear-venta" || $_GET["ruta"] == "editar-venta" || $_GET["ruta"] == "reporte-venta"){
				echo '<li class="treeview active">
						<a href="">
							<i class="fa fa-list-ul"></i>
								<span>Ventas</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
						</a>';
					

			}else{

				echo '<li class="treeview">
						<a href="">
							<i class="fa fa-list-ul"></i>
								<span>Ventas</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
						</a>';
					
			}
		}
			?>

			<ul class="treeview-menu">
						<li>
							<a href="ventas">
								<i class="fa fa-circle-o"></i>
								<span>Administrar Ventas</span>
							</a>
						</li>
						<li>
							<a href="crear-venta">
								<i class="fa fa-circle-o"></i>
								<span>Crear venta</span>
							</a>
						</li>
						<li>
							<a href="reporte-venta">
								<i class="fa fa-circle-o"></i>
								<span>Reporte de ventas</span>
							</a>
						</li>
					</ul>
				</li>
		</ul>

	</section>

</aside>