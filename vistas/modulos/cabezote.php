<header class="main-header">
	
	<!--=====================================
	=            LOGOTIPO          =
	======================================-->
	
	<a href="inicio" class="logo">
		
		<!-- logo mini -->
		<span class="logo-mini">
			
			<img src="vistas/img/plantilla/icono-blanco.png" class="img-responsive" style="padding:10px">
		
		</span>
		

		<!-- logo normal -->

		<span class="logo-lg">
			
			<img src="vistas/img/plantilla/logo-blanco-lineal.png" class="img-responsive" style="padding:10px 0px">
		
		</span>
	</a>

	<!--=====================================
	=            BARRA DE NAVEGACION          =
	======================================-->
	<nav class="navbar navbar-static-top" role="navigation">
		
		<!-- Boton de navegacion -->

		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only"> Toggle navigation</span>
		</a>

		<!-- perfil de usuario -->
		<div class="navbar-custom-menu">

			<ul class="nav navbar-nav">

	          <li class="dropdown user user-menu">

	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

	            		<?php

							if($_SESSION["foto"] != ""){

								echo '<img src="'.$_SESSION["foto"].'" class="user-image">';

							}else{

								echo '<img src="vistas/img/usuarios/default/anonimo.png" class="user-image">';

							}
						?>	
							<span class="hidden-xs"><?php echo $_SESSION["nombre"]?></span>
						</a>

	              			<span class="hidden-xs"></span>

            			</a>

	            	<ul class="dropdown-menu">

		              <!-- User image -->

		              <li class="user-header">

		               		<?php
								if($_SESSION["foto"] != ""){
									echo '<img src="'.$_SESSION["foto"].'" class="user-image">';
								}else{
									echo '<img src="vistas/img/usuarios/default/anonimo.png" class="user-image">';
								}
							?>	

		                <p>

		                 <?php echo $_SESSION["nombre"]?> - <?php echo $_SESSION["perfil"]?>

		                </p>

		              </li>

		              <!-- Menu Footer-->

		               <li class="user-footer">

			                <div class="pull-left">

			                  <a href="perfil" class="btn btn-default btn-flat">Profile</a>

			                </div>

			                <div class="pull-right">

			                  <a href="salir" class="btn btn-default btn-flat">Sign out</a>

			                </div>

		            	</li>

	            	</ul>
			</ul>

		</div>

	</nav>

</header>