<?php

  if($_SESSION["perfil"] == "Vendedor"){

    echo '<script>
          window.location = "inicio";
          </script>';

          return ; 
  }
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">

      <h1>

        Administrar Categorias

      </h1>

      <ol class="breadcrumb">

        <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

        <li class="active">Administrar Categorias</li>

      </ol>

    </section>

    <section class="content">

      <!-- Default box -->
      <div class="box">

        <div class="box-header with-border">

            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCategoria">
              Agregar Categoria
            </button>

        </div>

        <div class="box-body">

            <table class="table table-bordered table-striped tablas dt-responsive" width="100%">
              
                <thead>
                  
                    <tr>
                      <th style="width:10px">#</th>
                      <th>Nombre</th>
                      <th>Acciones</th>
                    </tr>

                </thead>
                <tbody>
                  
                  <?php

                  $item = null;
                  $valor = null;

                  $categorias = ControladorCategoria::ctrMostrarCategoria($item, $valor);

                  foreach($categorias as $key => $value){

                    echo '<tr>
                          <td>'.($key+1).'</td>
                          <td>'.$value["categoria"].'</td>
                          <td>
                            
                            <div class="btn-group">
                              
                             <button class="btn btn-warning btnEditarCategoria" idCategoria="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarCategoria"><i class="fa fa-pencil"></i></button>';

                            if($_SESSION["perfil"] == "Administrador"){

                             echo  '<button class="btn btn-danger btnEliminarCategoria" idCategoria="'.$value["id"].'">
                              <i class="fa fa-times"></i></button>';
                            }
                            
                          echo  '</div>

                          </td>

                        </tr>';
                  }

                  ?>

                </tbody>

            </table>

        </div>

      </div>

    </section>

  </div>

  <!--=====================================
  =       MODAL AGREGAR CATEGORIA            =
  ======================================-->
  
<div id="modalAgregarCategoria" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

          <!--=====================================
          =          CABEZA DEL MODAL            =
          ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar Categoria</h4>

        </div>

         <!--=====================================
          =         CUERPO DEL MODAL            =
          ======================================-->

        <div class="modal-body">

         <div class="box-body">

            <!-- ENTRADA PARA LA NUEVA CATEGORIA -->
           
            <div class="form-group">
              
              <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-th"></i></span>

                  <input type="text" class="form-control input-lg" name="nuevaCategoria" id="nuevaCategoria" placeholder="Ingresar categoria" required>

              </div>

            </div>

          </div>
        </div>

        <!-- PIE DE LAS ENTRADAS -->

        <div class="modal-footer">

          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar Cambios</button>

        </div>

        <?php

          $crearCategoria = new ControladorCategoria();
          $crearCategoria -> ctrCrearCategoria();

        ?>

      </form>

    </div>

  </div>

</div>


  <!--=====================================
  =       MODAL AGREGAR CATEGORIA            =
  ======================================-->
  
<div id="modalEditarCategoria" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

          <!--=====================================
          =          CABEZA DEL MODAL            =
          ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar Categoria</h4>

        </div>

         <!--=====================================
          =         CUERPO DEL MODAL            =
          ======================================-->

        <div class="modal-body">

         <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->
           
            <div class="form-group">
              
              <div class="input-group">
                
                  <span class="input-group-addon"><i class="fa fa-th"></i></span>

                  <input type="text" class="form-control input-lg" name="editarCategoria" id="editarCategoria" placeholder="Editar categoria" required>

                  <input type="hidden" name="idCategoria" id="idCategoria">

              </div>

            </div>

          </div>
        </div>

        <!-- PIE DE LAS ENTRADAS -->

        <div class="modal-footer">

          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

        <?php

          $crearCategoria = new ControladorCategoria();
          $crearCategoria -> ctrEditarCategoria();

        ?>

      </form>

    </div>

  </div>

</div>

<?php

          $eliminarCategoria = new ControladorCategoria();
          $eliminarCategoria -> ctrEliminarCategoria();

        ?>
  
  