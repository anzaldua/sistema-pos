<?php
require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

class AjaxProductos{

/*=============================================
= CREAR CODIGO A PARTIR DE OD CATEGORIA  =
=============================================*/
	
	public $idCategoria;
	public function ajaxCrearCodigoProducto(){

	$item = "id_categoria";
	$valor = $this->idCategoria;
	$orden = "id";

	$respuesta = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);

	echo json_encode($respuesta);
	}

/*=============================================
EDITAR PRODUCTO
=============================================*/ 

	  public $idProducto;
	  public $traerProductos;
	  public $nombreProducto;

	  public function ajaxEditarProducto(){

	  	if($this->traerProductos == "ok"){

		    $item = null;
		    $valor = null;
		    $orden = "id";

		    $respuesta = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);

		    echo json_encode($respuesta);

	  	}else if ($this->nombreProducto != "") {
	  		
	  		$item = "descripcion";
		    $valor = $this->nombreProducto;
		    $orden = "id";

		    $respuesta = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);

		    echo json_encode($respuesta);

	  	}else{

		    $item = "id";
		    $valor = $this->idProducto;
		    $orden = "id";

		    $respuesta = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);

		    echo json_encode($respuesta);
		}
	}
}

/*=============================================
= CREAR CODIGO A PARTIR DE OD CATEGORIA  =
=============================================*/

if(isset($_POST["idCategoria"])){

	$codigoProducto = new AjaxProductos();
	$codigoProducto -> idCategoria = $_POST["idCategoria"];
	$codigoProducto -> ajaxCrearCodigoProducto();

}

/*=============================================
EDITAR PRODUCTO
=============================================*/ 

if(isset($_POST["idProducto"])){

  $editarProducto = new AjaxProductos();
  $editarProducto -> idProducto = $_POST["idProducto"];
  $editarProducto -> ajaxEditarProducto();

}

/*=============================================
TRAER PRODUCTOs
=============================================*/ 

if(isset($_POST["traerProductos"])){

  $editarProducto = new AjaxProductos();
  $editarProducto -> traerProductos = $_POST["traerProductos"];
  $editarProducto -> ajaxEditarProducto();

}

/*=============================================
NOMBRE PRODUCTO
=============================================*/ 

if(isset($_POST["nombreProducto"])){

  $editarProducto = new AjaxProductos();
  $editarProducto -> nombreProducto = $_POST["nombreProducto"];
  $editarProducto -> ajaxEditarProducto();

}
