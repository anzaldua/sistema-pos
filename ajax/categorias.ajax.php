<?php

	require_once "../controladores/categorias.controlador.php";
	require_once "../modelos/categorias.modelo.php";

class AjaxCategoria{

/*=============================================
=            EDITAR CATEGORIA           =
=============================================*/

		public $idCategoria;

			public function ajaxEditarCategoria(){


				$item = "id";
				$valor = $this->idCategoria;
				$respuesta = ControladorCategoria::ctrMostrarCategoria($item, $valor);
				echo json_encode($respuesta);
			}

/*=============================================
=            VALIDAR NO REPETIR USUARIO           =
=============================================*/
		
		public $validarCategoria;

			public function ajaxValidarCategoria(){
				$item = "categoria";
				$valor = $this->validarCategoria;

					$respuesta = ControladorCategoria::ctrMostrarCategoria($item, $valor);

				echo json_encode($respuesta);
			}
	}

/*=============================================
=            VALIDAR NO REPETIR USUARIO           =
=============================================*/

if(isset($_POST["validarCategoria"])){

			$valCategoria = new AjaxCategoria();
			$valCategoria -> validarCategoria = $_POST["validarCategoria"];
			$valCategoria -> ajaxValidarCategoria();
		}

/*=============================================
=            EDITAR CATEGORIA           =
=============================================*/
if(isset($_POST["idCategoria"])){

			$categoria = new AjaxCategoria();
			$categoria -> idCategoria = $_POST["idCategoria"];
			$categoria -> ajaxEditarCategoria();
		}